From: Arnaud Ferraris <arnaud.ferraris@collabora.com>
Date: Mon, 10 Oct 2022 15:01:21 +0200
Subject: main: improve handling of external batteries

Batteries from connected devices (e.g. wireless keyboard/mouse) are
reported as external batteries, but have different sysfs attributes. As
an example, my Logitech wireless mouse has `online` instead of
`present`, `capacity_level` (as a string) instead of `capacity`,
`manufacturer` and `model_name` instead of `device/name`, and lacks
the `health` attribute.

With such devices, `powersupply` raises an exception when parsing the
corresponding battery properties and may end up failing to display any
meaningful data at all (tested on a laptop running Debian with python
3.10.7).

In order to make `powersupply` more tolerant to such cases, this commit:
* adds multiple checks on sysfs attributes' existence
* looks at `online` if `present` doesn't exist, defaulting to `True` if
  none of those is found
* falls back to reading `capacity_level` if `capacity` doesn't exist; this
  implied formatting the capacity string while parsing the battery
  attributes as `capacity_level` is a free-form string, while `capacity`
  is an integer representing the battery charge percentage.

Fixes #2, #3

Forwarded: https://gitlab.com/MartijnBraam/powersupply/-/merge_requests/8
---
 powersupply/__main__.py | 25 +++++++++++++++++++++----
 1 file changed, 21 insertions(+), 4 deletions(-)

diff --git a/powersupply/__main__.py b/powersupply/__main__.py
index f79f3b1..611f6b5 100644
--- a/powersupply/__main__.py
+++ b/powersupply/__main__.py
@@ -208,6 +208,12 @@ class DataPoller(threading.Thread):
                 if os.path.isfile(os.path.join(ebat, 'device/name')):
                     with open(os.path.join(ebat, 'device/name'), 'r') as handle:
                         name = handle.read().strip()
+                elif os.path.isfile(os.path.join(ebat, 'model_name')):
+                    with open(os.path.join(ebat, 'model_name'), 'r') as name_handle:
+                        name = name_handle.read().strip()
+                    if os.path.isfile(os.path.join(ebat, 'manufacturer')):
+                        with open(os.path.join(ebat, 'manufacturer'), 'r') as manuf_handle:
+                            name = '{} {}'.format(manuf_handle.read().strip(), name)
 
                 names = {
                     'kb151': 'Keyboard case'
@@ -217,19 +223,30 @@ class DataPoller(threading.Thread):
 
                 ext = ExternalBattery()
                 ext.name = name
+                ext.present = True
+                ext.health = 'N/A'
 
                 ebat_present = os.path.join(ebat, 'present')
+                ebat_online = os.path.join(ebat, 'online')
                 ebat_status = os.path.join(ebat, 'status')
                 ebat_health = os.path.join(ebat, 'health')
                 ebat_capacity = os.path.join(ebat, 'capacity')
+                ebat_capacity_level = os.path.join(ebat, 'capacity_level')
                 ebat_voltage = os.path.join(ebat, 'voltage_now')
                 ebat_current = os.path.join(ebat, 'current_now')
                 ebat_temp = os.path.join(ebat, 'temp')
 
-                ext.present = self.read_sysfs_str(ebat_present) == '1'
+                if os.path.isfile(ebat_present):
+                    ext.present = self.read_sysfs_str(ebat_present) == '1'
+                elif os.path.isfile(ebat_online):
+                    ext.present = self.read_sysfs_str(ebat_online) == '1'
                 ext.status = self.read_sysfs_str(ebat_status)
-                ext.health = self.read_sysfs_str(ebat_health) or 'N/A'
-                ext.capacity = self.read_sysfs(ebat_capacity)
+                if os.path.isfile(ebat_health):
+                    ext.health = self.read_sysfs_str(ebat_health)
+                if os.path.isfile(ebat_capacity):
+                    ext.capacity = '{}%'.format(self.read_sysfs(ebat_capacity))
+                elif os.path.isfile(ebat_capacity_level):
+                    ext.capacity = self.read_sysfs_str(ebat_capacity_level)
                 if os.path.isfile(ebat_voltage):
                     ext.voltage = self.read_sysfs(ebat_voltage) / 1000000.0
                 if os.path.isfile(ebat_current):
@@ -469,7 +486,7 @@ class Handler:
 
         for ext in result.ext:
             content = {
-                'Capacity': f'{ext.capacity}%',
+                'Capacity': ext.capacity,
             }
             if ext.voltage is not None:
                 content['Voltage'] = f'{ext.voltage}V'
